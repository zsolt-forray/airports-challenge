FROM python:3

ADD requirements.txt /
RUN pip install -r requirements.txt

ADD airports.py /

CMD [ "python", "./airports.py" ]
