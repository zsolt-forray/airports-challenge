# airports-challenge

[![Python 3.7](https://img.shields.io/badge/python-3.7-blue.svg)](https://www.python.org/downloads/release/python-370/)

## Description
Pulls data about airports from a database and displays their location in a simple list, based on their distances within user defined radius from the user defined latitude and longitude points.

### Usage Example

#### Create list of airports

```python
import airports as ap

boundary_box = (45, 50, 15, 20)
res = ap.run(ulat=47.49801, ulon=19.03991, bbox=boundary_box, radius=200, order="asc")
```

#### Parameters:
+   boundary_box: airports are retrieved within boundaries specified by `(lower latitude, higher latitude, lower longitude, higher longitude)` values;
+   ulat: user defined latitude position;
+   ulon: user defined longitude position;
+   bbox: user defined latitude and longitude boundaries, see `boundary_box`;
+   radius: search for airports within this radius (in km) from the position defined by `ulat` and `ulon` values;
+   order: list of airports is sorted in `asc` - ascending, `desc` - descending order. If not specified, unordered list is returned.

### Output
List of airports (sorted or unsorted)

![Screenshot](/png/air_out.png)

## LICENSE
MIT
