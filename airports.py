#!/usr/bin/python3

# File name: airports.py
# Author: Zsolt Forray
# Date created: 31/10/2019
# Python version: 3.7

"""
Application to retreive airports from a database within the user defined
latitude/longitude boundaries. Those airports which are within the user defined
radius from the user provided lat/long positions selected and added to a list.
"""

import json
import math
import requests


# Queries airport database within the defined latitude and longitude boundaries
def query_data(lat_low, lat_high, lon_low, lon_high):
    URL = r"https://mikerhodes.cloudant.com/airportdb/_design/view1/_search/geo?"
    params = dict(q=f"lon:[{lon_low} TO {lon_high}] AND lat:[{lat_low} TO {lat_high}]")

    req = requests.get(url=URL, params=params, timeout=10)
    json_data = req.json()
    return json_data

# Creates 'raw_dict' dictionary storing name of airports and their latitude/longitude positions
def pull_data2_dict(data):
    raw_dict = {}
    for i in data["rows"]:
        lat = i["fields"]["lat"]
        lon = i["fields"]["lon"]
        name = i["fields"]["name"]
        raw_dict[name] = [lat, lon]
    return raw_dict

# Haversine-formula to calculate distances beween GPS points
# https://www.geeksforgeeks.org/haversine-formula-to-find-distance-between-two-points-on-a-sphere/
def haversine(coord1, coord2):
    lat1, lon1 = coord1
    lat2, lon2 = coord2
    # distance between latitudes
    # and longitudes
    dLat = (lat2 - lat1) * math.pi / 180.0 # from decimal degrees to radians
    dLon = (lon2 - lon1) * math.pi / 180.0 # from decimal degrees to radians

    # convert to radians
    lat1 = (lat1) * math.pi / 180.0 # from decimal degrees to radians -> math.radians(lat1)
    lat2 = (lat2) * math.pi / 180.0 # from decimal degrees to radians -> math.radians(lat2)

    # apply formula
    a = (pow(math.sin(dLat / 2), 2) +
         pow(math.sin(dLon / 2), 2) *
             math.cos(lat1) * math.cos(lat2));
    rad = 6372.8
    c = 2 * math.asin(math.sqrt(a))
    return rad * c

# Calculates distances and add those values to 'raw_dict' dictionary
def add_distance(ulat, ulon, raw_dict):
    for i in raw_dict.keys():
        lat = raw_dict[i][0]
        lon = raw_dict[i][1]
        dist = haversine((ulat, ulon), (lat, lon))
        raw_dict[i].append(dist)
    return raw_dict

# Returns the ascending airport list
def get_asc_list(raw_dict, radius, order):
    full_list = sorted(raw_dict.items(), key=lambda x: x[1][2], reverse=False)

    i = 0
    res_list = []
    while i < len(full_list) and full_list[i][1][2] <= radius:
        res_list.append(full_list[i][0])
        i += 1
    return res_list

# Returns the descending airport list
def get_desc_list(raw_dict, radius, order):
    res_list = get_asc_list(raw_dict, radius, order)[::-1]
    return res_list

# Returns the unsorted airport list
def get_unsorted_list(raw_dict, radius):
    full_list = list(raw_dict.items())
    res_list = [i[0] for i in full_list if i[1][2] <= radius]
    return res_list

# Runs the codes
def run(ulat, ulon, bbox, radius, order=None):
    data = query_data(*bbox)
    raw_dict = pull_data2_dict(data)
    raw_dict = add_distance(ulat, ulon, raw_dict)

    if order == "asc":
        res_list = get_asc_list(raw_dict, radius, order)
    elif order == "desc":
        res_list = get_desc_list(raw_dict, radius, order)
    else:
        res_list = get_unsorted_list(raw_dict, radius)
    return res_list


if __name__ == "__main__":
    # boundary_box: (lower latitude, higher latitude, lower longitude, higher longitude)
    boundary_box = (45, 50, 15, 20)
    res = run(ulat=47.49801, ulon=19.03991, bbox=boundary_box, radius=200, order="asc")
    print(res)
